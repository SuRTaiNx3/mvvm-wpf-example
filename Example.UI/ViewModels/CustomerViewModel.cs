﻿using Example.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.UI.ViewModels
{
    public class CustomerViewModel : ViewModelBase
    {
        #region Globals

        private Customer _customer;

        #endregion

        #region Properties

        
        public string Name
        {
            get { return _customer.Name; }
            set
            {
                _customer.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Address
        {
            get { return $"{_customer.Street} {_customer.ZipCode} {_customer.City}"; }
        }


        public string Street
        {
            get { return _customer.Street; }
            set
            {
                _customer.Street = value;
                OnPropertyChanged("Street");
            }
        }

        public int ZipCode
        {
            get { return _customer.ZipCode; }
            set
            {
                _customer.ZipCode = value;
                OnPropertyChanged("ZipCode");
            }
        }

        public string City
        {
            get { return _customer.City; }
            set
            {
                _customer.City = value;
                OnPropertyChanged("City");
            }
        }


        private ObservableCollection<ContactViewModel> _contacts;
        public ObservableCollection<ContactViewModel> Contacts
        {
            get { return _contacts; }
            set { _contacts = value; }
        }


        #endregion

        #region Constructor

        public CustomerViewModel(Customer customer)
        {
            _customer = customer;

            Contacts = new ObservableCollection<ContactViewModel>();
            foreach (Contact contact in customer.Contacts)
                Contacts.Add(new ContactViewModel(contact));
        }

        #endregion

        #region Methods

        #endregion
    }
}
