﻿using Example.UI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Example.UI.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Globals

        private ContentControl _pageHoster;

        // Pages
        private ResourceDictionary _customerListPage;
        private ResourceDictionary _customerDetailPage;

        // ViewModels
        private CustomerListViewModel _customerListViewModel;
        private CustomerViewModel _customerViewModel;

        #endregion

        #region Properties

        #endregion

        #region Constructor

        public MainViewModel(ContentControl pageHoster)
        {
            _pageHoster = pageHoster;
            InitializePages();

            ShowCustomerList();
        }

        #endregion

        #region Commands

        private RelayCommand<CustomerViewModel> _customerSelectedCommand;
        public ICommand CustomerSelectedCommand
        {
            get
            {
                if (_customerSelectedCommand == null)
                    _customerSelectedCommand = new RelayCommand<CustomerViewModel>(a => ShowCustomerDetails(a), b => true);
                return _customerSelectedCommand;
            }
        }

        #endregion

        #region Methods

        private void ShowCustomerList()
        {
            _pageHoster.Content = _customerListPage["CustomerListPage"];

            if (_customerListViewModel == null)
            {
                _customerListViewModel = new CustomerListViewModel();
                _customerListViewModel.OnShowCustomerDetails += (object sender, CustomerViewModel selectedCustomer) =>
                {
                    ShowCustomerDetails(selectedCustomer);
                };
            }

            _pageHoster.DataContext = _customerListViewModel;
        }

        private void ShowCustomerDetails(CustomerViewModel customer)
        {
            _pageHoster.Content = _customerDetailPage["CustomerDetailsPage"];
            _pageHoster.DataContext = customer;

        }

        private void InitializePages()
        {
            // CustomerList
            _customerListPage = new ResourceDictionary();
            _customerListPage.Source = new Uri("/Example.UI;component/Pages/CustomerList.xaml", UriKind.RelativeOrAbsolute);

            // CustomerDetail
            _customerDetailPage = new ResourceDictionary();
            _customerDetailPage.Source = new Uri("/Example.UI;component/Pages/CustomerDetails.xaml", UriKind.RelativeOrAbsolute);
        }

        #endregion
    }
}
