﻿using Example.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.UI.ViewModels
{
    public class ContactViewModel : ViewModelBase
    {
        #region Globals

        private Contact _contact;

        #endregion

        #region Properties

        #endregion

        #region Constructor

        public ContactViewModel(Contact contact)
        {
            _contact = contact;
        }

        #endregion

        #region Methods

        #endregion
    }
}
