﻿using Example.Models;
using Example.UI.Common;
using Example.UI.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Example.UI.ViewModels
{
    public class CustomerListViewModel : ViewModelBase
    {
        #region Globals

        #endregion

        #region Properties

        private ObservableCollection<CustomerViewModel> _customers;
        public ObservableCollection<CustomerViewModel> Customers
        {
            get { return _customers; }
            set
            {
                _customers = value;
                OnPropertyChanged("Customers");
            }
        }

        public CustomerViewModel SelectedCustomer { get; set; }


        #endregion

        #region Events

        public delegate void OnShowCustomerDetailsEventHandler(object sender, CustomerViewModel customerViewModel);
        public event OnShowCustomerDetailsEventHandler OnShowCustomerDetails;

        #endregion

        #region Constructor

        public CustomerListViewModel()
        {
            GenerateTestData();
        }

        #endregion

        #region Methods

        private void GenerateTestData()
        {
            Customers = new ObservableCollection<CustomerViewModel>()
            {
                new CustomerViewModel(new Customer("Valve Corporation", "IrgendwoInUSA 55", 13377, "Bellevue", new List<Contact>(){
                    new Contact("Gabe", "Newell", "CEO"),
                    new Contact("Mike", "Ambinder", "Game Design")
                })),
                new CustomerViewModel(new Customer("Ubisoft", "WasWeißIch 98", 19454, "Paris", new List<Contact>(){
                    new Contact("Yves", "Guillemot", "CEO"),
                    new Contact("Serge", "Hascoet", "CCO"),
                })),
                new CustomerViewModel(new Customer("Electronic Arts, Inc.", "IrgendwoInUSA 1561", 56292, "Redwood City", new List<Contact>(){
                    new Contact("Andrew", "Wilson", "CEO"),
                    new Contact("Larry", "Probst", "Chairman"),
                    new Contact("Peter", "Moore", "COO")
                }))
            }.ToObservableCollection();
        }

        #endregion
    }
}
