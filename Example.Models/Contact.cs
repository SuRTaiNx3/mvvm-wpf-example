﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.Models
{
    public class Contact
    {
        #region Globals

        #endregion

        #region Properties

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Function { get; set; }

        #endregion

        #region Constructor

        public Contact(){}

        public Contact(string firstName, string lastName, string function)
        {
            FirstName = firstName;
            LastName = lastName;

            Function = function;
        }

        #endregion

        #region Methods

        #endregion
    }
}
