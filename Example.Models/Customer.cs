﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Example.Models
{
    public class Customer
    {
        #region Globals

        #endregion

        #region Properties

        public string Name { get; set; }

        public string Street { get; set; }
        public int ZipCode { get; set; }
        public string City { get; set; }

        public List<Contact> Contacts { get; set; }

        #endregion

        #region Constructor

        public Customer(){}

        public Customer(string name, string street, int zipCode, string city)
        {
            Name = name;
            Street = street;
            ZipCode = zipCode;
            City = city;
        }

        public Customer(string name, string street, int zipCode, string city, List<Contact> contacts)
            :this(name, street, zipCode, city)
        {
            Contacts = contacts;
        }

        #endregion

        #region Methods

        #endregion
    }
}
